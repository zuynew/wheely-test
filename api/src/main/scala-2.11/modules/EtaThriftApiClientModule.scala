package wheely.api.modules

import com.twitter.finagle.Thrift
import com.typesafe.config.Config
import wheely.core.thriftscala.EtaThrift

trait EtaThriftApiClientModule {

  def config: Config

  lazy val etaThriftServiceClient =
    Thrift.newIface[EtaThrift.FutureIface](config.getString("client.host") + ":" + config.getString("client.port"))

}
