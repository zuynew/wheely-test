package wheely.api.modules

import com.softwaremill.macwire._
import wheely.api.service.EtaRestService
import wheely.core.thriftscala.EtaThrift

trait RestServiceModule {

  def etaThriftServiceClient: EtaThrift.FutureIface

  lazy val etaRestService = wire[EtaRestService]

}
