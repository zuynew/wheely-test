package wheely.api.service

case class Point(latitude: Double, longitude: Double)
