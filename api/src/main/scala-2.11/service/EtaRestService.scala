package wheely.api.service

import io.finch.{Endpoint, _}
import io.finch.circe._
import io.circe.generic.auto._

class EtaRestService(client: wheely.core.thriftscala.EtaThrift.FutureIface) {

  def eta: Endpoint[EtaResult] = {

    implicit def pointToThriftPoint(
        position: Point):  wheely.core.thriftscala.Point = {
      wheely.core.thriftscala.Point(position.latitude, position.longitude)
    }

    implicit def etaResultToThriftEtaResult(
        etaResult:  wheely.core.thriftscala.EtaResult): EtaResult = {
      EtaResult(etaResult.eta)
    }

    post("eta" :: body.as[Point]) { point: Point =>
      Ok(client.calculate(point).map(etaResultToThriftEtaResult))
    } handle {
      case e:  wheely.core.thriftscala.IllegalPosition => BadRequest(e)
      case e: Exception => InternalServerError(e)
    }
  }
}

object EtaRestService {
  def apply(client:  wheely.core.thriftscala.EtaThrift.FutureIface): EtaRestService = new EtaRestService(client)
}
