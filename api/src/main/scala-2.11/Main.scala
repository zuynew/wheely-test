package wheely.api

import com.twitter.finagle.Http
import com.twitter.util.Await
import io.finch.circe._
import io.circe.generic.auto._
import wheely.api.modules._
import wheely.core.modules.ConfigModule

object Main
    extends App
    with ConfigModule
    with EtaThriftApiClientModule
    with RestServiceModule {

  Await.ready(Http.server.serve(config.getString("server.host") + ":" + config.getString("server.port"), etaRestService.eta.toService))

}
