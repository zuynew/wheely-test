package wheely.thrift.modules

import com.softwaremill.macwire._
import wheely.core.ExtendedPostgresDriver
import wheely.core.domain.dao.CarDao

import scala.concurrent.ExecutionContext

trait DaoModule {

  implicit def ec: ExecutionContext
  def db: ExtendedPostgresDriver.API#Database

  lazy val carDao = wire[CarDao]

}
