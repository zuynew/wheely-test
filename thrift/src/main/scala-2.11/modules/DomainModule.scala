package wheely.thrift.modules

import com.softwaremill.macwire._
import wheely.core.domain.HarvesineEtaCalculator
import wheely.core.domain.dao.CarDao

import scala.concurrent.ExecutionContext

trait DomainModule {

  implicit def ec: ExecutionContext
  def carDao: CarDao

  lazy val etaCalculator = wire[HarvesineEtaCalculator]

}
