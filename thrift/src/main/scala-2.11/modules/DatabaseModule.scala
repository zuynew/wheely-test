package wheely.thrift.modules

import com.typesafe.config.Config
import wheely.core.ExtendedPostgresDriver.api._

trait DatabaseModule {

  def config: Config

  lazy val db = Database.forConfig("database", config)

}
