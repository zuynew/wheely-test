package wheely.thrift.modules

import com.softwaremill.macwire._
import wheely.core.domain.EtaCalculator
import wheely.core.service.EtaService

import scala.concurrent.ExecutionContext

trait ServiceModule {

  implicit def ec: ExecutionContext
  def etaCalculator: EtaCalculator

  lazy val etaService = wire[EtaService]

}
