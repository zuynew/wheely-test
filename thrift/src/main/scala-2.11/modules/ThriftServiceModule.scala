package wheely.thrift.modules

import com.softwaremill.macwire._
import wheely.core.service.EtaService
import wheely.thrift.thriftscala.service.EtaThriftService

import scala.concurrent.ExecutionContext

trait ThriftServiceModule {

  implicit def ec: ExecutionContext
  def etaService: EtaService

  lazy val etaThriftService = wire[EtaThriftService]

}
