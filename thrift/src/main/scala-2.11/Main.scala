package wheely.thrift

import com.twitter.finagle.Thrift
import com.twitter.util.Await
import wheely.core.modules._
import wheely.thrift.modules._

import scala.concurrent.ExecutionContext

object Main
    extends App
    with ConfigModule
    with DatabaseModule
    with DaoModule
    with DomainModule
    with ServiceModule
    with ThriftServiceModule {

  implicit def ec: ExecutionContext = ExecutionContext.global

  val server = Thrift.serveIface(
      config.getString("server.host") + ":" + config.getString("server.port"),
      etaThriftService)

  Await.ready(server)
}
