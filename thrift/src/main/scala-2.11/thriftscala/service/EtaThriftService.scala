package wheely.thrift.thriftscala.service

import com.twitter.bijection.Conversion.asMethod
import com.twitter.util.{Future, Return, Throw, Try}
import com.vividsolutions.jts.geom.{Coordinate, GeometryFactory}
import wheely.core.service.EtaService
import wheely.core.thriftscala._

import scala.concurrent.ExecutionContext

class EtaThriftService(etaService: EtaService)(implicit ec: ExecutionContext)
    extends EtaThrift[Future] {

  override def calculate(position: Point): Future[EtaResult] = {
    import com.twitter.bijection.twitter_util.UtilBijections._

    implicit def positionReqToPoint(
        positionReq: Point): com.vividsolutions.jts.geom.Point = {
      new GeometryFactory().createPoint(
          new Coordinate(positionReq.latitude, positionReq.longitude))
    }

    if (!(
            (-90 < position.latitude) && (position.latitude < 90)
              && (-180 < position.longitude) && (position.longitude < 180)
        )) {
      throw IllegalPosition("Illegal position")
    }

    etaService.calculate(position).as[Future[Double]].map(EtaResult(_))

  }

}

object EtaThriftService {
  def apply(etaService: EtaService)(
      implicit ec: ExecutionContext): EtaThriftService = new EtaThriftService(etaService)
}
