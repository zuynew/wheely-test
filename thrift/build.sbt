name := "wheely-thrift"

libraryDependencies ++= Seq(
  "com.typesafe.slick" %% "slick" % "3.1.1",
  "com.typesafe.slick" % "slick-hikaricp_2.11" % "3.1.1",
  "org.slf4j" % "slf4j-nop" % "1.6.4"
)


//libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.1.7"

libraryDependencies += "com.github.tminglei" %% "slick-pg" % "0.14.1"

libraryDependencies += "com.github.tminglei" %% "slick-pg_jts" % "0.14.1"

libraryDependencies ++= Seq(
  "org.apache.thrift" % "libthrift" % "0.8.0",
  "com.twitter" %% "scrooge-core" % "4.6.0",
  "com.twitter" %% "finagle-thrift" % "6.34.0",
  "com.twitter" % "bijection-util_2.11" % "0.9.2"
)


//libraryDependencies += "net.debasishg" %% "redisclient" % "3.0"

libraryDependencies += "com.softwaremill.macwire" %% "macros" % "2.2.3" % "provided"

libraryDependencies += "com.softwaremill.macwire" %% "util" % "2.2.3"

libraryDependencies += "com.softwaremill.macwire" %% "proxy" % "2.2.3"

