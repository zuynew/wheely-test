namespace java wheely.core.thriftjava
#@namespace scala wheely.core.thriftscala

struct Point {
 1: double latitude,
 2: double longitude
}

struct EtaResult {
    1: double eta
}

exception IllegalPosition {
  1: string description;
}

service EtaThrift {
  EtaResult calculate(1: Point position)
    throws(1: IllegalPosition ex)
}
