package wheely.core.domain

import com.vividsolutions.jts.geom.Point
import wheely.core.domain.dao.CarDao

import scala.concurrent.{ExecutionContext, Future}

class HarvesineEtaCalculator(carDao: CarDao)(implicit ec: ExecutionContext)
    extends EtaCalculator {

  val SAMPLE_SIZE = 3

  override def calculate(position: Point): Future[Double] =
    for {
      cars <- carDao.findNearestAvailable(position, SAMPLE_SIZE)
      etaList <- Future(cars.map(car =>
                          Haversine.apply(position, car.position) * 1.5))
      eta <- Future(etaList.sum / etaList.length)
    } yield eta
}

object HarvesineEtaCalculator {
  def apply(carDao: CarDao)(
      implicit ec: ExecutionContext): HarvesineEtaCalculator =
    new HarvesineEtaCalculator(carDao: CarDao)(ec)
}
