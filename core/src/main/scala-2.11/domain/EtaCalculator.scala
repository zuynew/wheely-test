package wheely.core.domain

import com.vividsolutions.jts.geom.Point

import scala.concurrent.Future

trait EtaCalculator {
  def calculate(position: Point): Future[Double]
}
