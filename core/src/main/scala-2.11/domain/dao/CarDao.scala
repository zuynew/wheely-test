package wheely.core.domain.dao

import com.vividsolutions.jts.geom.Point
import wheely.core.ExtendedPostgresDriver.api._
import wheely.core.domain.models.{Car, CarResultGetter, CarTable}

import scala.concurrent.{ExecutionContext, Future}

class CarDao(db: Database)(implicit ec: ExecutionContext) {

  private val cars = TableQuery[CarTable]

  def all(): Future[Seq[Car]] = db.run(cars.result)

  def findNearestAvailable(position: Point, limit: Int) = {
    import CarResultGetter._

    db.run(
        sql"SELECT * FROM tbl_car c WHERE is_available = TRUE ORDER BY c.position <-> $position::geography LIMIT $limit"
          .as[Car])
  }

}

object CarDao {
  def apply(db: Database)(implicit ec: ExecutionContext): CarDao =
    new CarDao(db: Database)(ec)
}
