package wheely.core.domain

import com.vividsolutions.jts.geom.Point

import scala.math._

object Haversine {
  val R = 6372.8 //radius in km

  def apply(lat1: Double, lon1: Double, lat2: Double, lon2: Double): Double = {
    val dLat = (lat2 - lat1).toRadians
    val dLon = (lon2 - lon1).toRadians

    val a = pow(sin(dLat / 2), 2) + pow(sin(dLon / 2), 2) * cos(lat1.toRadians) * cos(
          lat2.toRadians)
    val c = 2 * asin(sqrt(a))
    R * c
  }

  def apply(point1: Point, point2: Point): Double = {
    apply(point1.getX, point1.getY, point2.getX, point2.getY)
  }

}
