package wheely.core.domain.models

import com.vividsolutions.jts.geom.Point
import slick.jdbc.GetResult
import wheely.core.ExtendedPostgresDriver.api._

case class Car(id: Option[Int] = None, position: Point, isAvailable: Boolean)

object CarResultGetter {
  implicit val getCarResult = GetResult(
      r => Car(r.nextIntOption(), r.nextGeometry[Point](), r.nextBoolean()))
}

class CarTable(tag: Tag) extends Table[Car](tag, "tbl_car") {
  def id = column[Int]("id", O.AutoInc, O.PrimaryKey)

  def position = column[Point]("position")

  def isAvailable = column[Boolean]("is_available")

  def * = (id.?, position, isAvailable) <> (Car.tupled, Car.unapply)
}
