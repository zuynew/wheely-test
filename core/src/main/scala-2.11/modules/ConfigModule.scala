package wheely.core.modules

import com.typesafe.config.{Config, ConfigFactory}

trait ConfigModule {

  def config: Config = ConfigFactory.load()
}
