package wheely.core

import com.github.tminglei.slickpg._
import slick.driver.JdbcProfile
import slick.profile.Capability

trait ExtendedPostgresDriver extends ExPostgresDriver with PgPostGISSupport {
  def pgjson =
    "jsonb" // jsonb support is in postgres 9.4.0 onward; for 9.3.x use "json"

  // Add back `capabilities.insertOrUpdate` to enable native `upsert` support; for postgres 9.5+
  override protected def computeCapabilities: Set[Capability] =
    super.computeCapabilities + JdbcProfile.capabilities.insertOrUpdate

  override val api = ExtendedAPI

  object ExtendedAPI
      extends API
      with PostGISImplicits
      with PostGISPlainImplicits

}

object ExtendedPostgresDriver extends ExtendedPostgresDriver
