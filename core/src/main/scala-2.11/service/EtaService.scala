package wheely.core.service

import com.vividsolutions.jts.geom.Point
import wheely.core.domain.EtaCalculator

import scala.concurrent.{ExecutionContext, Future}

class EtaService(etaCalculator: EtaCalculator)(implicit ec: ExecutionContext) {

  def calculate(position: Point): Future[Double] = {
    etaCalculator.calculate(position)
  }

}

object EtaService {
  def apply(etaCalculator: EtaCalculator)(
      implicit ec: ExecutionContext): EtaService =
    new EtaService(etaCalculator)(ec)
}
