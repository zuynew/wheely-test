

lazy val commonSettings = Seq(
  version := "0.1.0",
  scalaVersion := "2.11.8"
)


lazy val core = (project in file("core")).
  settings(commonSettings: _*)

lazy val api = (project in file("api")).
  settings(commonSettings: _*).
  dependsOn(core)

lazy val thrift = (project in file("thrift")).
  settings(commonSettings: _*).
  dependsOn(core)

