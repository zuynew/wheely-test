Wheely Eta service
============


Требования
------------

* java 7+
* postgres 9.3+ + postgis


Запуск
-----

Необходимо запустить postgres с раширением postgis. 
Для этих целей возможно использовать докер образ файл которого лежит в etc/docker/postgres

Что бы создать образ необходимо выполнить:

```bash
cd etc/docker/postgres
docker build -t postgres-postgis:latest .
docker run --rm -p 5432:5432 postgres-postgis:latest
```

Для создания схемы необходино выполнить:

```bash
cd etc/
./shmig.sh -t postgresql -d postgres -l postgres -p root -H localhost  migrate
```

Для сборки проекта выполнить

```bash
sbt assembly
```

Запускать без параметров или же указав путь для application.conf

Запуск thrift api
```bash
java -jar thrift/target/scala-2.11/wheely-thrift-assembly-0.1.0.jar
```

Запуск rest api
```bash
java -jar api/target/scala-2.11/wheely-api-assembly-0.1.0.jar
```


Пример запроса
-----

```
Запрос

POST /eta

{"latitude": 21.09, "longitude": 13.93}

Ответ:

{
    "eta": 23
}

```


Почему такой стек
-----

Postgresql с postgis хорошо зарекомендовавшее себя решения для задач по работе с гео данными

Slick как эталонный вариант для работы с БД

MacWire потому что предпочитаю "constructor injection" "cake pattern'у"

Твиттер стек для апи известен своим удобством и возможностью держать большие нагрузки

Thrift выбран как удобный бинарный протокол out-of-box для finagle 

Finch мега удобная функциональная обертка на finagle 