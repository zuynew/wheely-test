-- Migration: cars
-- Created at: 2016-06-30 22:19:53
-- ====  UP  ====

BEGIN;

INSERT INTO tbl_car (position, is_available)
VALUES ('POINT(55.760534 37.612104)', TRUE ),
  ('POINT(47.224053 39.714847)', TRUE ),
  ('POINT(55.761705 37.635193)', TRUE ),
  ('POINT(55.850564 37.355140)', TRUE ),
  ('POINT(55.825334 37.755896)', FALSE ),

COMMIT;

-- ==== DOWN ====

BEGIN;

COMMIT;
