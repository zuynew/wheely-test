-- Migration: tbl_car
-- Created at: 2016-06-30 22:07:13
-- ====  UP  ====

BEGIN;
CREATE TABLE tbl_car
(
    id BIGINT PRIMARY KEY NOT NULL,
    position GEOGRAPHY(POINT,4326) NOT NULL,
    is_available BOOLEAN NOT NULL
);

CREATE INDEX tbl_car_position ON tbl_car USING GIST ("position");

COMMIT;

-- ==== DOWN ====

BEGIN;

COMMIT;
